/*Ex.1: "Créer une variable avec du texte dedans puis faire une condition
    pour afficher la valeur de la variable en console dans le cas où
    cette variable contient "bloup"*/

let text='bloup';
if(text==='bloup') {
    console.log(text);
}

/*Ex.2: "La même chose avec une variable numérique, et cette fois on
    affichera sa valeur seulement si la valeur est supérieure à 10"*/

let exo1=20;
if(exo1>10) {
    console.log('Bigger than 10')
};

/*Ex.3: "Faire un autre if qui affichera un truc si la variable numérique
    a une valeur entre 5 et 10 non compris"*/

let exoNum=6;
if(5<exoNum && exoNum<10){
    console.log("Between 5 and 10")
};

/*Ex.4: "Faire encore un autre if qui affichera un truc si la variable
    numérique a une valeur entre 20 et 100 compris ou vaut 15"*/

let number=30;
if(20<=number && number<=100 || number===15) {
    console.log("Something");
};