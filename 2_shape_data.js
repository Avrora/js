let form = document.querySelector("form");
let playground = document.querySelector("#playground");

form.addEventListener("submit", function(event) {
    event.preventDefault();

    // let data = new FormData(form);

    let valuesCreated = {
        width: document.querySelector("#width").value,
        height: document.querySelector("#height").value,
        color: document.querySelector("#color").value,
        roundness: document.querySelector("#roundness").value,
    };
    // console.log(valuesCreated);
    
    let shapeCreated = new Shape(valuesCreated.width,                                               
                                valuesCreated.height,
                                valuesCreated.color, 
                                valuesCreated.roundness, 
                                130, 
                                130);    
    console.log(shapeCreated.draw());
    
    let stockDraw = shapeCreated.draw();
    playground.appendChild(stockDraw);


});

